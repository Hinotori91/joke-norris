package isi.code.jokenorris.data

interface JokeAPI {
    suspend fun random(category: String? = null): Joke
    suspend fun categories(): List<String>
    suspend fun search(query: String): MutableList<SearchObject>
}