package isi.code.jokenorris.data

data class Result(
  val total: Int,
  val result: MutableList<SearchObject>
)
