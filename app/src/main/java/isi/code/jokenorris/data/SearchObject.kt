package isi.code.jokenorris.data

data class SearchObject(
  val categories: MutableList<String>,
  val created_ad: String,
  val icon_url: String,
  val id: String,
  val updated_ad: String,
  val url: String,
  val value: String,
)
