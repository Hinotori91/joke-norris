package isi.code.jokenorris.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class JokeRepository(
    private val delegate: JokeAPI,
) : JokeAPI by delegate {
    override suspend fun random(category: String?): Joke = withContext(Dispatchers.IO) {
        delegate.random(category)
    }

    override suspend fun categories(): List<String> = withContext(Dispatchers.IO) {
        delegate.categories()
    }

    override suspend fun search(query: String): MutableList<SearchObject> = withContext(Dispatchers.IO) {
        delegate.search(query)
    }
}