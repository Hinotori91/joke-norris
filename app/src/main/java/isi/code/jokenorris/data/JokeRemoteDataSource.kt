package isi.code.jokenorris.data

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.android.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.bodyAsText
import io.ktor.serialization.gson.*

class JokeRemoteDataSource : JokeAPI {
    override suspend fun random(category: String?): Joke =
        HttpClient.get("https://api.chucknorris.io/jokes/random") {
            category?.let { url { parameters.append("category", category) } }
        }.body()

    override suspend fun categories(): List<String> =
        HttpClient.get("https://api.chucknorris.io/jokes/categories").body()

    override suspend fun search(query: String): MutableList<SearchObject> {
        val result = HttpClient.get("https://api.chucknorris.io/jokes/search"){
            parameter("query", query)
        }.body() as Result

        return result.result
    }

    companion object {
        private val HttpClient by lazy {
            HttpClient(Android) {
                install(ContentNegotiation) {
                    gson()
                }
            }
        }
    }
}