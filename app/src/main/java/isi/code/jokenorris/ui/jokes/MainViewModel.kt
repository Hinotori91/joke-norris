package isi.code.jokenorris.ui.jokes

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import isi.code.jokenorris.data.JokeRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class MainViewModel(
    private val repository: JokeRepository,
    private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

    var uiState by mutableStateOf(MainUiState())
        private set

    var verbs = sequence { while (true) yieldAll(listOf("say", "do", "think", "like")) }
        .asFlow()
        .onEach { delay(2000) }

    init {
        viewModelScope.launch {
            uiState = uiState.copy(categories = repository.categories())
        }
    }

    fun search(query: String){
        viewModelScope.launch {
            uiState = uiState.copy(searchList = repository.search(query))
        }
    }
}

