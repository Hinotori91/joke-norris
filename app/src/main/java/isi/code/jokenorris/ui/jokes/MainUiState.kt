package isi.code.jokenorris.ui.jokes

import isi.code.jokenorris.data.Joke
import isi.code.jokenorris.data.Result
import isi.code.jokenorris.data.SearchObject

data class MainUiState(
  val categories: List<String> = listOf(),
  val searchList: MutableList<SearchObject> = mutableListOf()
)