package isi.code.jokenorris.ui.joke

import isi.code.jokenorris.data.Joke

data class JokeUiState(
    val joke: Joke? = null,
    val category: String? = null,
)