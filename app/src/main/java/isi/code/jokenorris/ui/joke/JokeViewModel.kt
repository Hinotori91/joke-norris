package isi.code.jokenorris.ui.joke

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import isi.code.jokenorris.data.JokeRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class JokeViewModel(
  private val repository: JokeRepository,
  private val savedStateHandle: SavedStateHandle,
) : ViewModel() {

  var uiState by mutableStateOf(JokeUiState())
    private set

  var refreshing by mutableStateOf(false)
    private set

  var category: String?
    get() = uiState.category
    set(value) {
      println(value)
      uiState = uiState.copy(category = value)
    }

  private var fetchJob: Job? = null

  init {
    refresh()
  }

  fun refresh() {
    fetchJob?.cancel()
    fetchJob = viewModelScope.launch {
      refreshing = true
      uiState = uiState.copy(joke = repository.random(uiState.category))
      refreshing = false
    }
  }
}

