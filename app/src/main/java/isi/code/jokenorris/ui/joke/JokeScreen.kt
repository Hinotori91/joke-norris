package isi.code.jokenorris.ui.joke

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Surface
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Alignment.Companion.TopCenter
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import isi.code.jokenorris.data.JokeRemoteDataSource
import isi.code.jokenorris.data.JokeRepository
import isi.code.jokenorris.ui.theme.Purple40
import isi.code.jokenorris.ui.theme.Purple80
import isi.code.jokenorris.ui.theme.Typography
import kotlin.text.Typography.leftGuillemet
import kotlin.text.Typography.rightGuillemet

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun JokeScreen(
    navController: NavHostController,
    vm: JokeViewModel = viewModel {
        JokeViewModel(
            repository = JokeRepository(JokeRemoteDataSource()),
            savedStateHandle = createSavedStateHandle()
        )
    },
    category: String?,
) {
    vm.category = category
    val pullRefreshState = rememberPullRefreshState(vm.refreshing, vm::refresh)
    Box(
        modifier = Modifier.pullRefresh(pullRefreshState)
    ) {
        LazyColumn(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = CenterHorizontally
        ) {
            vm.uiState.joke?.let {
                item {
                    Surface(
                        modifier = Modifier.align(Center),
                        shape = RoundedCornerShape(15.dp),
                        color = Purple80,
                    ) {
                        Box(
                            modifier = Modifier.padding(10.dp)
                        ) {
                            Text(
                                buildAnnotatedString {
                                    withStyle(SpanStyle(Purple40)) {
                                        append(rightGuillemet)
                                    }
                                    append(it.value)
                                    withStyle(SpanStyle(Purple40)) {
                                        append(leftGuillemet)
                                    }
                                }, style = Typography.titleLarge
                            )
                        }
                    }
                }
            }
        }
        PullRefreshIndicator(vm.refreshing, pullRefreshState, Modifier.align(TopCenter))
    }
}

