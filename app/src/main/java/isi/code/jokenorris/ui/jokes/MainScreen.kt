package isi.code.jokenorris.ui.jokes

import android.content.ClipData.Item
import androidx.compose.animation.*
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Arrangement.spacedBy
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.staggeredgrid.LazyHorizontalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Chip
import androidx.compose.material.ChipDefaults.chipColors
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.IconButton
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.BottomEnd
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import isi.code.jokenorris.data.JokeAPI
import isi.code.jokenorris.data.JokeRemoteDataSource
import isi.code.jokenorris.data.JokeRepository
import isi.code.jokenorris.ui.joke.JokeUiState
import isi.code.jokenorris.ui.theme.Typography
import kotlin.text.Typography.bullet

@OptIn(
    ExperimentalMaterialApi::class,
    ExperimentalFoundationApi::class,
    ExperimentalAnimationApi::class
)
@Composable
fun MainScreen(
    navController: NavHostController,
    vm: MainViewModel = viewModel {
        MainViewModel(
            repository = JokeRepository(JokeRemoteDataSource()),
            savedStateHandle = createSavedStateHandle()
        )
    },
) {
    val verbs by vm.verbs.collectAsState(initial = bullet.toString().repeat(3))

    Column {
        Box(
            modifier = Modifier
                .fillMaxWidth(0.6f)
                .align(CenterHorizontally)
        ) {
            Column(horizontalAlignment = CenterHorizontally) {
                Text("What would", style = Typography.titleLarge)
                Text(
                    "Chuck Norris",
                    style = Typography.titleLarge,
                    fontWeight = FontWeight.SemiBold
                )
                Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Center) {
                    AnimatedContent(targetState = verbs,
                        transitionSpec = { slideInVertically { -it } with slideOutVertically { it } }) {
                        Text(it, style = Typography.titleLarge)
                    }
                }
            }
            Box(modifier = Modifier.align(BottomEnd)) {
                Text("?", style = Typography.titleLarge)
            }
        }

        Spacer(modifier = Modifier.height(30.dp))

        LazyHorizontalStaggeredGrid(
            modifier = Modifier
                .height(250.dp)
                .fillMaxWidth(),
            rows = StaggeredGridCells.Adaptive(40.dp),
            verticalArrangement = spacedBy(5.dp),
            horizontalArrangement = spacedBy(5.dp),
            userScrollEnabled = false
        ) {
            items(vm.uiState.categories) {
                Chip(
                    onClick = { navController.navigate("joke/$it") },
                    colors = chipColors(backgroundColor = Color(it.hashCode()).copy(alpha = 0.5f))
                ) {
                    Text(it)
                }
            }
        }

        Spacer(modifier = Modifier.size(15.dp))
        
        var searchText by remember {mutableStateOf("")}
        Row {

            TextField(
                value = searchText,
                onValueChange = { searchText = it },
                label = { Text("Search Word") },
                modifier = Modifier.clip(RoundedCornerShape(10.dp))
            )

            IconButton(onClick = {
                vm.search(searchText)
            }) {
                Icon(Icons.Default.Search, contentDescription = "Search")
            }
        }

//        Text(text = vm.uiState.searchList)
            LazyColumn{
                items(vm.uiState.searchList){
                    Card {
                        Text(text = it.value)
                    }
                    Spacer(modifier = Modifier.size(5.dp))
                }
            }

    }
}

