package isi.code.jokenorris

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import isi.code.jokenorris.ui.joke.JokeScreen
import isi.code.jokenorris.ui.jokes.MainScreen
import isi.code.jokenorris.ui.theme.JokeNorrisTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JokeNorrisTheme {
                Surface {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(20.dp)
                    ) {
                        val navController = rememberNavController()
                        NavHost(navController, "jokes") {
                            composable(route = "jokes") {
                                MainScreen(navController)
                            }
                            composable(
                                route = "joke/{category}",
                                arguments = listOf(navArgument("category") {
                                    type = NavType.StringType
                                })
                            ) {
                                JokeScreen(
                                    navController,
                                    category = it.arguments?.getString("category")
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}